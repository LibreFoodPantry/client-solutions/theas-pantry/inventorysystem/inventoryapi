# InventoryAPI

Repository to store API development
progress for the InventorySystem component
of Theas Pantry.
The entry point of the specification is in `src/index.yaml`

## 1. Developer Guide

### 1.1 Install development environment

To avoid dependency conflicts across projects, and to reduce variance
between development platforms, we use VS Code devcontainers.

New to VS Code devcontainers? Start here
[https://code.visualstudio.com/docs/remote/containers]
and follow its installation instructions. Be sure to install and
configure Git too.

Now download, install, and run this project and its devcontainer as
follows.

1. Navigate to this project on GitLab and select
    `Clone -> Open in your IDE -> Visual Studio Code (HTTPS)`.
2. Select location to store the project.
3. Select "Reopen in container" when option is provided.

### 1.2. Validate specification

```bash
npm run validate
```

### 1.3. Bundle specification into a single file

```bash
npm run bundle outfile.yaml
```

For example, when creating a release 0.2.4 ...

```bash
npm run bundle bundles/items-api.0.2.4.yaml
```
